1. Create a new SSH key pair locally with ssh-keygen
2. Add the private key as a variable to your project
3. Run the ssh-agent during job to load the private key.
4. Copy the public key to the servers you want to have access to (usually in `~/.ssh/authorized_keys`) or add it as a deploy key if you are accessing a private GitLab repository.

- Generate private key
```bash
ssh-keygen
```

- Copy private key and paste in your gitlab project:
`Settings > CI/CD > variables
```bash
cat cp .ssh/id_rsa
```

- Copy the public key to the server
```bash
cp .ssh/id_rsa.pub ~/.ssh/authorized_keys
chmod 600 ~/.ssh/authorized_keys
```

### permissions ssh no password
```bash
echo "username  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/username
```

##### example .gitlab-ci.yml

```yaml
image: php:7.4-fpm
stages:
  - stage
  - deploy

before_script:
  - apt-get update -qq
  - apt-get install -qq git
  - "which ssh-agent || ( apt-get install -qq openssh-client )"
  - eval $(ssh-agent -s)
  - ssh-add <(echo "$SSH_PRIVATE_KEY")
  - mkdir -p ~/.ssh
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'

deployment:
  stage: deploy
  cache:
    paths:
      - src/vendor
  script:
    - |
      ssh ${SSH_USER}@${SSH_HOST} \
      "
      cd /home/ckam/www/project_name \
      && git checkout master \
      && git pull origin master \
      && docker-compose -f docker-compose.yml -f \
      docker-compose.production.yml build \
      && docker-compose -f docker-compose.yml -f \
      docker-compose.production.yml up -d \
      && exit \
      "
  only:
    - master
```

#### example 2
```yaml
image: ubuntu:latest
variables:
  WORK_DIR: ${CI_PROJECT_NAME}
  BRANCH: ${CI_COMMIT_REF_NAME}
  REPO: https://gitlab.com/johandurancerdas/gitlab-cicd-tutorial.git
stages:
  - test
  - deploy

test:
  stage: test
  environment: 
    name: Test
    url: "$TST_URL"
  before_script:
  - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
  - mkdir -p ~/.ssh
  - eval $(ssh-agent -s)
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    - ssh-add <(echo "$PRIVATE_KEY")
    - rm -rf .git
    - ssh -o StrictHostKeyChecking=no ubuntu@"$TST_SERVER" "rm -rf ~/${WORK_DIR}; mkdir ~/${WORK_DIR}; git init; git clone -b ${BRANCH} ${REPO}; cd ${WORK_DIR}; npm install; npm install forever -g; npm run stop; npm run ci"
  only:
    - branches
  except:
    - master
    
    
deploy:
  stage: deploy
  environment: 
    name: Production
    url: "$PRD_URL"
  before_script:
  - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
  - mkdir -p ~/.ssh
  - eval $(ssh-agent -s)
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    - ssh-add <(echo "$PRIVATE_KEY")
    - rm -rf .git
    - ssh -o StrictHostKeyChecking=no ubuntu@"$PRD_SERVER" "rm -rf ~/${WORK_DIR}; mkdir ~/${WORK_DIR}; git init; git clone -b ${BRANCH} ${REPO}; cd ${WORK_DIR}; npm install; npm install forever -g; npm run stop; npm run start-background"
  only:
    - master

```