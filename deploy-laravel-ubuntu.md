Prerequisites


## Installing and Configuring Redis

[source](https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-20-04)

Begin by updating your local apt package cache:
```bash
sudo apt update
```
Then install Redis by typing:
```bash
sudo apt install redis-server
```
Open this file with your preferred text editor:

```bash
sudo nano /etc/redis/redis.conf
```

`/etc/redis/redis.conf`

```bash
. . .

# If you run Redis from upstart or systemd, Redis can interact with your
# supervision tree. Options:
#   supervised no      - no supervision interaction
#   supervised upstart - signal upstart by putting Redis into SIGSTOP mode
#   supervised systemd - signal systemd by writing READY=1 to $NOTIFY_SOCKET
#   supervised auto    - detect upstart or systemd method based on
#                        UPSTART_JOB or NOTIFY_SOCKET environment variables
# Note: these supervision methods only signal "process is ready."
#       They do not enable continuous liveness pings back to your supervisor.
supervised systemd

. . .
```
Restart Redis
```bash
sudo systemctl restart redis.service
```