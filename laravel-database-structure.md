```php
Route::get('/test', function () {
    $sql = "SELECT table_name FROM information_schema.tables WHERE table_schema='public';";

    $tables = DB::select($sql);
    $output = [];
    foreach ($tables as $table) {
        $sql2 = "select column_name, data_type, character_maximum_length, column_default, is_nullable
        from INFORMATION_SCHEMA.COLUMNS where table_name = ?;";
        // dd($table);
        $columns = DB::select($sql2, [$table->table_name]);
        $output[] = [
            'table' => $table->table_name,
            'columns' => $columns
        ];
    }
    return response()->json($output);
});
```
