# Marketplace

#### Requirements

-   REDIS: `statble`
-   POSTGRESQL: `12`
-   COMPOSER
-   NGINX
-   PHP: `7.4`
-   PHP-FPM

#### clone project repository to nginx folder

```bash
cd /var/www/html/ && sudo mkdir -p your_project_folder
sudo chown $USER:$USER your_project_folder
cd your_project_folder && git clone git@gitlab.com:05ru/marketplace.git # or extract archive file into this directory
git clone --single-branch --branch master git@bitbucket.org:ckam255/marketplace05.git
```

### Nginx config example

```bash
# /etc/nginx/nginx.conf
http {
  ...

# for laravel

server {
    listen 80;
    server_name example.com;
    root /srv/example.com/public;

    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    index index.php;

    charset utf-8;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    error_page 404 /index.php;

    location ~ \.php$ {
        fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~ /\.(?!well-known).* {
        deny all;
    }
}
```

#### install dependencies

```bash
 composer install
```

#### copy .env file

```bash
cp .env.example .env
```

-   environment variables in `.env`

```bash
APP_DEBUG=false
APP_NAME=Marketplace05
APP_ENV=production
APP_URL=http://domaine_name

FRONTEND_URL = http://localhost:8080
RESET_PASSWORD_FRONTEND_URL = "${FRONTEND_URL}/auth/password/reset"
```

-   Change database connection parameters in `.env`

```BASH
DB_CONNECTION=pgsql
DB_HOST=localhost
DB_PORT=5432
DB_DATABASE=dbname
DB_USERNAME=username
DB_PASSWORD=secret
```

-   Change redis parameters in `.env`

```BASH
QUEUE_CONNECTION=redis


REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379
```

-   STMP server for sending mails `.env`

```bash
MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=tls
MAIL_FROM_ADDRESS=noreply@example.com
MAIL_FROM_NAME="${APP_NAME}"
```

#### generate app key

```bash
php artisan key:generate
```

#### fix permissions

```bash
sudo chown -R $USER:$USER /var/www/yourdomain/html
sudo chgrp -R www-data storage bootstrap/cache
sudo chmod -R ug+rwx storage bootstrap/cache
sudo chmod -R 777 storage 
sudo chmod -R 777 bootstrap/cache
```

#### migrate database

```bash
php artisan migrate
```

#### Background jobs

-   Run default queue

```bash
php artisan queue:work --tries=3
```

### Optimization


#### Autoloader Optimization
```bash
composer install --optimize-autoloader --no-dev
```

#### Optimizing Route Loading
```bash
php artisan route:cache
```

#### Optimizing Configuration Loading
```bash
php artisan config:cache
```