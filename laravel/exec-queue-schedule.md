docker-compose.yml

```yaml
version: "3"
services:
  app:
    image: laravel-www
    container_name: laravel-www
    build:
      context: .
      dockerfile: docker/Dockerfile
    depends_on:
      - redis
      - mysql
    ports:
      - 8080:80
    volumes:
      - .:/var/www/html
    environment:
      APP_ENV: local
      CONTAINER_ROLE: app
      CACHE_DRIVER: redis
      SESSION_DRIVER: redis
      QUEUE_DRIVER: redis
      REDIS_HOST: redis

  scheduler:
    image: laravel-www
    container_name: laravel-scheduler
    depends_on:
      - app
    volumes:
      - .:/var/www/html
    environment:
      APP_ENV: local
      CONTAINER_ROLE: scheduler
      CACHE_DRIVER: redis
      SESSION_DRIVER: redis
      QUEUE_DRIVER: redis
      REDIS_HOST: redis

  queue:
    image: laravel-www
    container_name: laravel-queue
    depends_on:
      - app
    volumes:
      - .:/var/www/html
    environment:
      APP_ENV: local
      CONTAINER_ROLE: queue
      CACHE_DRIVER: redis
      SESSION_DRIVER: redis
      QUEUE_DRIVER: redis
      REDIS_HOST: redis

  redis:
    container_name: laravel-redis
    image: redis:4-alpine
    ports:
      - 16379:6379
    volumes:
      - redis:/data

  mysql:
    container_name: laravel-mysql
    image: mysql:5.7
    ports:
      - 13306:3306
    volumes:
      - mysql:/var/lib/mysql
    environment:
      MYSQL_DATABASE: homestead
      MYSQL_ROOT_PASSWORD: root
      MYSQL_USER: homestead
      MYSQL_PASSWORD: secret

volumes:
  redis:
    driver: "local"
  mysql:
    driver: "local"
```

entrypoint.sh
```bash
#!/usr/bin/env bash

set -e

role=${CONTAINER_ROLE:-app}
env=${APP_ENV:-production}

if [ "$env" != "local" ]; then
    echo "Caching configuration..."
    (cd /var/www/html && php artisan config:cache && php artisan route:cache && php artisan view:cache)
fi

if [ "$role" = "app" ]; then

    exec apache2-foreground

elif [ "$role" = "queue" ]; then

    echo "Queue role"
    exit 1

elif [ "$role" = "scheduler" ]; then

    echo "Scheduler role"
    exit 1

else
    echo "Could not match the container role \"$role\""
    exit 1
fi
```