### Step One — Create the Directory Structure
```bash
sudo mkdir -p /var/www/example.com/html
```

##### Grant Permissions
```bash
sudo chown -R $USER:$USER /var/www/example.com/html
```

We should also modify our permissions a little bit to ensure that read access is permitted to the general web directory, and all of the files and folders inside, so that pages can be served correctly:
```bash
sudo chmod -R 755 /var/www
```

##### Create Demo Pages for Site
```bash
nano /var/www/example.com/html/index.html
```
```html
<html>
  <head>
    <title>Welcome to Example.com!</title>
  </head>
  <body>
    <h1>Success! The example.com server block is working!</h1>
  </body>
</html>
```
### Step Three — Create New Server Block Files
Server block files are what specify the configuration of our separate sites and dictate how the Nginx web server will respond to various domain requests.

To begin, we will need to set up the directory that our server blocks will be stored in, as well as the directory that tells Nginx that a server block is ready to serve to visitors.
The `sites-available` directory will keep all of our server block files, while the `sites-enabled` directory will hold symbolic links to server blocks that we want to publish. We can make both directories by typing:

```bash
sudo mkdir /etc/nginx/sites-available
sudo mkdir /etc/nginx/sites-enabled
```
Next, we should tell Nginx to look for server blocks in the sites-enabled directory.
To accomplish this, we will edit Nginx’s main configuration file and add a line declaring an optional directory for additional configuration files:
```bash
sudo nano /etc/nginx/nginx.conf
```

Add these lines to the end of the http {} block:
```bash
include /etc/nginx/sites-enabled/*.conf;
server_names_hash_bucket_size 64;
```

Create the First Server Block File
By default, Nginx contains one server block called default.conf which we can use as a template for our own configurations. We can create our first server block config file by copying over the default file:
```bash
sudo cp /etc/nginx/conf.d/default.conf /etc/nginx/sites-available/example.com.conf
```
Now, open the new file in your text editor with root privileges:
```bash
sudo nano /etc/nginx/sites-available/example.com.conf
```

Note: Due to the configurations that we have outlined, all server block files must end in .conf.

Ignoring the commented lines, the file will look similar to this:
```bash
server {
    listen  80;
    server_name localhost;

    location / {
        root  /usr/share/nginx/html;
        index  index.html index.htm;
    }

    error_page  500 502 503 504  /50x.html;
    location = /50x.html {
        root  /usr/share/nginx/html;
    }
}
```
The first thing that we’re going to have to adjust is the server_name, which tells Nginx which requests to point to this server block. 
We’ll declare the main server name, example.com, as well as an additional alias to www.example.com, so that both www. and non-www. requests are served the same content:

`server_name example.com www.example.com;`



Note: Each Nginx statement must end with a semi-colon `(;)`, so check each of your statement lines if you are running into problems later on.

Next, we want to modify the document root, specified by the root directive. Point it to the site’s document root that you created:
```bash
root /var/www/example.com/html;
```
We’ll also want to add a `try_files` command that ends with a 404 error if the desired filename or directory is not found:

`try_files $uri $uri/ =404;`


When you are finished, your file will look something like this:
```bash
server {
    listen  80;

    server_name example.com www.example.com;

    location / {
        root  /var/www/example.com/html;
        index  index.html index.htm;
        try_files $uri $uri/ =404;
    }

    error_page  500 502 503 504  /50x.html;
    location = /50x.html {
        root  /usr/share/nginx/html;
    }
}
```

### Step Four — Enable the New Server Block Files

Now that we have created our server block files, we need to enable them so that Nginx knows to serve them to visitors. 
To do this, we can create a symbolic link for each server block in the sites-enabled directory:

```bash
sudo ln -s /etc/nginx/sites-available/example.com.conf /etc/nginx/sites-enabled/example.com.conf
```

When you are finished, restart Nginx to make these changes take effect:
```bash
sudo systemctl restart nginx
```