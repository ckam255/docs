# Set Up Nginx Server Blocks (Virtual Hosts) on CentOS 8



### Step 1: Create an Nginx Document Root Directory
Right off the bat, you need to create a custom web root directory for the domain you want to host. For our case, 
we will create the directory as shown using the `mkdir -p` option to create all the necessary parent directories:
```bash
sudo mkdir -p /var/www/your_domain/html
```
Thereafter assign the directory permissions using the `$USER` environment variable. As you do so, ensure that you are logged in as a regular user and not the root user.
```bash
sudo chown -R $USER:$USER /var/www/cyour_domain/html
```

Next, assign the right directory permissions recursively as shown:
```bash
$ sudo chmod -R 755 /var/www/your_domain/html
```


### Step 2: Create a Sample Page for the Domain


Next, we are going to create an `index.html` file inside the custom web root directory that will be served by the domain once a request is made.

```bash
sudo vim /var/www/your_domain/html/index.html
```
Inside the file, paste the following sample content.
```html
<html>
    <head>
        <title>Welcome to your_domain!</title>
    </head>
    <body>
  <h1>Awesome! Your Nginx server block is working!</h1>
    </body>
</html>
```

## Step 3: Create an Nginx Server Block in CentOS
For the Nginx web server to serve the content in the `index.html` file we created in step 2, we need to create a server block file with the appropriate directives. Therefore, we shall create a new server block at:
```bash
sudo vim /etc/nginx/conf.d/your_domain.conf
```
Next, paste the configuration that appears below.
```conf
server {
        listen 80;
        listen [::]:80;

        root /var/www/your_domain/html;
        index index.html index.htm index.nginx-debian.html;

        server_name your_domain www.your_domain;

        location / {
                try_files $uri $uri/ =404;
        }

		
    access_log /var/log/nginx/your_domain.access.log;
    error_log /var/log/nginx/your_domain.error.log;

}
```
When you are done, save the changes and exit the configuration file. To confirm that all Nginx configurations are sound and error-free, execute the command:
```bash
sudo nginx -t
```

Finally, restart your Nginx web server and confirm that it’s running as expected:
```bash
sudo systemctl restart nginx
sudo systemctl status Nginx
```

### Step 4: Testing the Nginx Server Block in CentOS
We are all done with the configurations. The only part remaining is to confirm if our server block is serving content in the web root directory defined earlier in the index.html file.

To do this, simply open your browser and go to your server’s domain as shown:
```bash
http://your_domain
```