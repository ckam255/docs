# Set Up Nginx Virtual Hosts on Ubuntu 20.04

[source doc](https://serverspace.io/support/help/nginx-virtual-hosts-on-ubuntu-20-04/)

##### Nginx configuration files

First, you need to install the Nginx package.

```bash
sudo apt install nginx
```

Create a folder for the website and place its files there.

```bash
sudo mkdir -p /var/www/domain-name.com
```

All configuration files for Nginx virtual hosts are stored in the `/etc/nginx/sites-available/` folder. The best way is to create a separate file for each web site on the server. Let’s create the first configuration for domain-name.com.

```bash
sudo nano /etc/nginx/sites-available/domain-name.com
```

Now insert this configuration there.

```bash
#

server {
    listen 80;
    listen [::]:80; # The same thing for IPv6
    server_name your_domain www.your_domain;
    root /var/www/your_domain;

    index index.html index.htm index.php;

    location / {
        try_files $uri $uri/ =404;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php7.4-fpm.sock; # or fastcgi_pass    127.0.0.1:9000; # sudo nano /etc/php/7.4/fpm/pool.d/www.conf
     }

    location ~ /\.ht {
        deny all;
    }

}
```

And set permissions for the folder.

```bash
sudo chown -R $USER:$USER /var/www/your_domain/html
sudo chmod -R 755 /var/www/your_domain/html
```

##### Enabling the Nginx virtual host

You need to create a symbolic link to the configuration in the `sites-enabled` directory to enable the virtual host.

```bash
sudo ln -s /etc/nginx/sites-available/domain-name.com /etc/nginx/sites-enabled/
```

Now check the configuration for errors.

```bash
sudo nginx -t
```

And restart the service.

```bash
sudo systemctl restart nginx
```

Now you have a working virtual host for a single domain. You can access it by domain name if the DNS server is configured correctly. Any number of domains can be added to the server in this way.

#####  Disabling Nginx virtual hosts
To disable a virtual host, remove the symbolic link from the sites-enabled folder. To disable returning a standard web page when accessing the server’s IP address, you can simply delete the link to the default configuration.

```bash
sudo rm /etc/nginx/sites-enabled/default
```

Restart the service after that.

```bash
sudo systemctl restart nginx
```

This way you can disable any configuration you need. And enable it by adding a symbolic link again, as we did earlier.
